/*!*********************************************************************************
 *  \file       behavior_catalog.h
 *  \brief      BehaviorCatalog definition file.
 *  \details    This file contains the BehaviorCatalog declaration.
 *              To obtain more information about it's definition consult
 *              the behavior_catalog.cpp file.
 *  \authors    Alberto Camporredondo.
 *  \copyright  Copyright 2017 Universidad Politecnica de Madrid (UPM)
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program. If not, see http://www.gnu.org/licenses/.
 ********************************************************************************/

#ifndef BEHAVIOR_CATALOG_H
#define BEHAVIOR_CATALOG_H

//System
#include <string>
#include <map>
#include <utility>
#include <cstdlib>
#include <algorithm>
#include <tuple>
#include <regex>
//ROS
#include <yaml-cpp/yaml.h>

//Aerostack
#include "../include/argument_descriptor.h"
#include "../include/capability_descriptor.h"
#include "../include/behavior_descriptor.h"
#include "../include/reactive_behavior_descriptor.h"
#include "../include/prettify.h"

class BehaviorCatalog
{
public:
  BehaviorCatalog();
  ~BehaviorCatalog();

private: //variables
  std::map<std::string, BehaviorDescriptor> behaviors_map;
  std::map<std::string, CapabilityDescriptor> capabilities_map;
  std::map<std::string, ReactiveBehaviorDescriptor> reactive_behaviors_map;
  std::string config_file_path;
  Prettify prettify;

public: //functions
  bool loadConfiguration(std::string path_file);
  void eraseConfiguration();
  std::tuple<bool, std::string, BehaviorDescriptor> getBehaviorDescriptor(std::string name);
  std::tuple<bool, std::string, ReactiveBehaviorDescriptor> getReactiveBehaviorDescriptor(std::string name);
  std::tuple<bool, std::string, CapabilityDescriptor> getCapabilityDescriptor(std::string capability_name);
  std::tuple<bool, std::string, std::vector<CapabilityDescriptor> > getCapabilities(std::string behavior_name);
  std::vector<BehaviorDescriptor> getBehaviors();
  std::vector<CapabilityDescriptor> getDefaultCapabilityDescriptors();
  std::vector<ReactiveBehaviorDescriptor> getHighPriorityReactiveBehaviors();
  std::vector<ReactiveBehaviorDescriptor> getLowPriorityReactiveBehaviors();
  void checkProcesses(ros::V_string available_processes, std::string node_namespace);
  void checkBehaviors(ros::V_string available_behaviors, std::string node_namespace);
  std::vector<std::string> getProcessesInCatalog();
  std::vector<std::string> checkUnavailability(std::vector<std::string> processes_launched,
					       std::vector<std::string> processes_in_catalog);
  bool processIsLaunched(std::string process_in_catalog, std::vector<std::string> processes_launched);

private:
  void updateConfigurationFile();
};

#endif
