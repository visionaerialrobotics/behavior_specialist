/*!*********************************************************************************
 *  \file       prettify.h
 *  \brief      Prettify definition file.
 *  \details    This file contains the Prettify declaration.
 *              To obtain more information about it's definition consult
 *              the prettify.cpp file.
 *  \authors    Alberto Camporredondo.
 *  \copyright  Copyright 2017 Universidad Politecnica de Madrid (UPM)
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program. If not, see http://www.gnu.org/licenses/.
 ********************************************************************************/

#ifndef _PRETTIFY_H
#define _PRETTIFY_H

#include <string>
#include <iostream>
#include <algorithm>
#include <vector>

#define ERROR "\033[1;31m"
#define OK "\033[1;32m"
#define INFO "\033[1;97m"
#define STOP "\033[0m"
#define WARNING "\033[1;33m"

class Prettify
{
public:
  Prettify();
  ~Prettify();

public:
  void printParent(std::string operation, std::string message);
  void printChild(std::string operation, std::string message);
  void printChildList(std::string operation, std::string message, std::vector<std::string> elements);
  void printGrandChild(std::string operation, std::string message);
  void printGrandChildList(std::string operation, std::string message, std::vector<std::string> elements);
  void printError(std::string message);
  void printEnd(std::string message);
  void printTitle(std::string message);
  void printWarningVector(std::string message, std::vector<std::string>);

private:
  std::string getColor(std::string operation);

};

#endif
