/*!*********************************************************************************
 *  \file       reactive_behavior_descriptor.h
 *  \brief      ReactiveBehaviorDescriptor definition file.
 *  \details    This file contains the BehaviorCatalog declaration.
 *              To obtain more information about it's definition consult
 *              the reactive_behavior_descriptor.cpp file.
 *  \authors    Alberto Camporredondo.
 *  \copyright  Copyright 2017 Universidad Politecnica de Madrid (UPM)
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program. If not, see http://www.gnu.org/licenses/.
 ********************************************************************************/

#ifndef REACTIVE_BEHAVIOR_DESCRIPTOR_H
#define REACTIVE_BEHAVIOR_DESCRIPTOR_H

#include <string>

/*Priority definition*/
enum Priority {lower, higher};

/*Class definitions*/
class ReactiveBehaviorDescriptor
{
public:
  ReactiveBehaviorDescriptor();
  ReactiveBehaviorDescriptor(std::string name,
			     std::string arguments,
                             std::string condition,
                             std::string priority);
  ~ReactiveBehaviorDescriptor();

private:
  std::string name;
  std::string arguments;
  std::string condition;
  Priority priority;

private:
  Priority getPriorityAsInt(std::string);

public:
  std::string getName();
  std::string getArguments();
  std::string getCondition();
  Priority getPriority();
  bool hasLowerPriority();
  bool hasHigherPriority();
};

#endif
