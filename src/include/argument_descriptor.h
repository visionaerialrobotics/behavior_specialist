/*!*********************************************************************************
 *  \file       argument_descriptor.h
 *  \brief      ArgumentDescriptor definition file.
 *  \details    This file contains the ArgumentDescriptor declaration.
 *              To obtain more information about it's definition consult
 *              the argument_descriptor.cpp file.
 *  \authors    Alberto Camporredondo.
 *  \copyright  Copyright 2017 Universidad Politecnica de Madrid (UPM)
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program. If not, see http://www.gnu.org/licenses/.
 ********************************************************************************/

#ifndef ARGUMENT_DESCRIPTOR_H
#define ARGUMENT_DESCRIPTOR_H

#include <string>
#include <vector>
#include <tuple>
#include <sstream>
#include <iostream>
#include <cstddef>
#include <algorithm>

class ArgumentDescriptor
{
public:
  ArgumentDescriptor();
  ArgumentDescriptor(std::string, std::vector<std::string>, int);
  ~ArgumentDescriptor();

private:
  std::string name;
  std::vector<std::string> allowed_values;
  int dimensions;

public:
  /*Functionality*/
  bool check_name(std::string);
  std::tuple<bool, std::string> check_string_format(std::string argument_values);
  std::tuple<bool, std::string> check_int_format(std::vector<double> argument_values);
  std::tuple<bool, std::string> check_int_format(double argument_value);
  std::tuple<bool, std::string> check_variable_format(std::vector<std::string> argument_variable);

  /*Getters*/
  std::string getName();
  std::vector<std::string> getAllowedValues();
  int getDimensions();

  /*Setters*/
  void setName(std::string);
  void setAllowedValues(std::vector<std::string>);
  void setDimensions(int);
};

#endif
