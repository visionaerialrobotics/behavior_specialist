/*!*********************************************************************************
 *  \file       behavior_descriptor.h
 *  \brief      BehaviorDescriptor definition file.
 *  \details    This file contains the BehaviorCatalog declaration.
 *              To obtain more information about it's definition consult
 *              the behavior_descriptor.cpp file.
 *  \authors    Alberto Camporredondo.
 *  \copyright  Copyright 2017 Universidad Politecnica de Madrid (UPM)
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program. If not, see http://www.gnu.org/licenses/.
 ********************************************************************************/

#ifndef BEHAVIOR_DESCRIPTOR_H
#define BEHAVIOR_DESCRIPTOR_H

#include <utility>
#include <string>
#include <vector>

//Aerostack msgs
#include <aerostack_msgs/BehaviorCommand.h>

#include "argument_descriptor.h"


class BehaviorDescriptor
{

public:
  BehaviorDescriptor();
  BehaviorDescriptor(std::string name,
                     std::vector<std::string> capabilities,
                     std::vector<std::string> incompatibilities,
                     std::string category, int timeout);
  BehaviorDescriptor(std::string name,
                     std::vector<std::string> capabilities,
                     std::vector<std::string> incompatibilities,
                     std::string category, int timeout,
                     std::vector<ArgumentDescriptor> arguments);
  ~BehaviorDescriptor();

private:
  std::string name;
  int timeout;
  std::string category;
  std::vector<ArgumentDescriptor> arguments;
  std::vector<std::string> capabilities;
  std::vector<std::string> incompatibilities;

public:
  /*Functionality*/
  aerostack_msgs::BehaviorCommand serialize();

  /*Getter's*/
  std::string getName();
  std::vector<ArgumentDescriptor> getArguments();
  std::vector<std::string> getCapabilities();
  std::vector<std::string> getIncompatibilities();
  std::string getCategory();
  int getTimeout();

  /*Setter's*/
  void setName(std::string name);
  void setCategory(std::string value);
  //  void setArguments(std::string arguments);
  void setTimeout(int timeout);
  void setCapabilities(std::vector<std::string>);
  void setIncompatibilities(std::vector<std::string>);
};

#endif
