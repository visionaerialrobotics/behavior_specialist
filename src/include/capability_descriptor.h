/*!*********************************************************************************
 *  \file       capability_descriptor.h
 *  \brief      CapabilityDescriptor definition file.
 *  \details    This file contains the CapabilityDescriptor declaration.
 *              To obtain more information about it's definition consult
 *              the capability_descriptor.cpp file.
 *  \authors    Alberto Camporredondo.
 *  \copyright  Copyright 2017 Universidad Politecnica de Madrid (UPM)
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program. If not, see http://www.gnu.org/licenses/.
 ********************************************************************************/

#ifndef CAPABILITY_DESCRIPTOR_H
#define CAPABILITY_DESCRIPTOR_H

#include <string>
#include <vector>

#include <droneMsgsROS/Capability.h>

class CapabilityDescriptor
{
public:
  CapabilityDescriptor();
  CapabilityDescriptor(std::string name,
		       bool basic, std::vector<std::string>processes,
		       std::vector<std::string> incompatibilities);
  ~CapabilityDescriptor();

private:
  std::string name;
  bool basic;
  std::vector<std::string> processes;
  std::vector<std::string> incompatibilities;

public:
  void addProcess(std::string process);
  void addIncompatibility(std::string incompatibility);
  droneMsgsROS::Capability serialize();

  /*Getters*/
  std::string getName();
  bool isBasic();
  std::vector<std::string> getProcesses();
  std::vector<std::string> getIncompatibilities();

  /*Setters*/
  void setName(std::string name);
  void setBasic(bool value);
  void setProcesses(std::vector<std::string> processes_vector);
  void setIncompatibilities(std::vector<std::string> incompatibilities_vector);
};

#endif
