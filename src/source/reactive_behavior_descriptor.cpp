/*!*******************************************************************************************
 *  \file       reactive_behavior_descriptor.cpp
 *  \brief      ReactiveBehaviorDescriptor implementation file.
 *  \details    This file implements the ReactiveBehaviorDescriptor class.
 *  \authors    Alberto Camporredondo.
 *  \copyright  Copyright 2017 Universidad Politecnica de Madrid (UPM)
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program. If not, see http://www.gnu.org/licenses/.
 ********************************************************************************/

#include "../include/reactive_behavior_descriptor.h"

ReactiveBehaviorDescriptor::ReactiveBehaviorDescriptor()
{

}

ReactiveBehaviorDescriptor::ReactiveBehaviorDescriptor(std::string name,
						       std::string arguments,
                                                       std::string condition,
                                                       std::string priority)
{
  this->name = name;
  this->arguments = arguments;
  this->condition = condition;
  this->priority = getPriorityAsInt(priority);
}

ReactiveBehaviorDescriptor::~ReactiveBehaviorDescriptor()
{

}

/*Features*/
Priority ReactiveBehaviorDescriptor::getPriorityAsInt(std::string priority)
{
  if(priority == "higher")
    return higher;
  else
    return lower;
}

/*Getters*/
std::string ReactiveBehaviorDescriptor::getName()
{
  return name;
}

std::string ReactiveBehaviorDescriptor::getArguments()
{
  return arguments;
}

std::string ReactiveBehaviorDescriptor::getCondition()
{
  return condition;
}

Priority ReactiveBehaviorDescriptor::getPriority()
{
  return priority;
}

bool ReactiveBehaviorDescriptor::hasHigherPriority()
{
  return priority == higher;
}

bool ReactiveBehaviorDescriptor::hasLowerPriority()
{
  return priority == lower;
}
/*Setters*/
