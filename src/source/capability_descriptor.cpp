/*!*******************************************************************************************
 *  \file       capability_descriptor.cpp
 *  \brief      CapabilityDescriptor implementation file.
 *  \details    This file implements the CapabilityDescriptor class.
 *  \authors    Alberto Camporredondo.
 *  \copyright  Copyright 2017 Universidad Politecnica de Madrid (UPM)
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program. If not, see http://www.gnu.org/licenses/.
 ********************************************************************************/

#include "../include/capability_descriptor.h"

CapabilityDescriptor::CapabilityDescriptor()
{
  basic = false;
}

CapabilityDescriptor::CapabilityDescriptor(std::string name,
					   bool basic, std::vector<std::string> processes,
					   std::vector<std::string> incompatibilities)
{
  this->name = name;
  this->basic = basic;
  this->processes = processes;
  this->incompatibilities = incompatibilities;
}

CapabilityDescriptor::~CapabilityDescriptor()
{

}


void CapabilityDescriptor::addProcess(std::string process)
{
  processes.push_back(process);
}

void CapabilityDescriptor::addIncompatibility(std::string name)
{
  incompatibilities.push_back(name);
}

droneMsgsROS::Capability CapabilityDescriptor::serialize()
{
  droneMsgsROS::Capability capability_msg;
  capability_msg.name = name;
  capability_msg.basic = basic;
  capability_msg.processes = processes;

  return capability_msg;
}

/*Getters*/
std::string CapabilityDescriptor::getName()
{
  return name;
}

bool CapabilityDescriptor::isBasic()
{
  return basic;
}

std::vector<std::string> CapabilityDescriptor::getProcesses()
{
  return processes;
}

std::vector<std::string> CapabilityDescriptor::getIncompatibilities()
{
  return incompatibilities;
}

/*Setters*/
void CapabilityDescriptor::setName(std::string name)
{
  this->name = name;
}

void CapabilityDescriptor::setBasic(bool value)
{
  basic = value;
}

void CapabilityDescriptor::setProcesses(std::vector<std::string> processes_vector)
{
  processes = processes_vector;
}

void CapabilityDescriptor::setIncompatibilities(std::vector<std::string> incompatibilities_vector)
{
  incompatibilities = incompatibilities_vector;
}
