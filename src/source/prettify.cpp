/*!*******************************************************************************************
 *  \file       prettify.cpp
 *  \brief      Prettify implementation file.
 *  \details    This file implements the Prettify class.
 *  \authors    Alberto Camporredondo.
 *  \copyright  Copyright 2017 Universidad Politecnica de Madrid (UPM)
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program. If not, see http://www.gnu.org/licenses/.
 ********************************************************************************/

#include "../include/prettify.h"

Prettify::Prettify()
{

}

Prettify::~Prettify()
{

}

void Prettify::printParent(std::string operation, std::string message)
{
  std::cout << std::endl;
  std::cout << getColor(operation) << "=> " << message << STOP << std::endl;
}

void Prettify::printChild(std::string operation, std::string message)
{
  std::cout << STOP << "\t|" << std::endl;
  std::cout <<         "\t---> " << getColor(operation) << message << STOP << std::endl;
}

void Prettify::printChildList(std::string operation, std::string message, std::vector<std::string> elements)
{
  std::cout << STOP << "\t|" << std::endl;
  std::cout <<         "\t---> " << getColor(operation) << message << ": [ ";
  int i = 0, j = 0;
  std::string array_elements = "", line = "";
  for(auto element : elements){
   line += element;
   array_elements += element;
    i++; j++;
    if(i < elements.size()){
      array_elements += ", ";
      if(line.size() >= 20){
        array_elements += "\n\t\t\t\t  ";
        line = "";
        j = 0;
      }
    }
  }
  std::cout << array_elements << " ]" << STOP << std::endl;
}

void Prettify::printGrandChild(std::string operation, std::string message)
{
  std::cout << STOP << "\t\t|" << std::endl;
  std::cout <<         "\t\t---> " << getColor(operation) << message << STOP << std::endl;
}

void Prettify::printGrandChildList(std::string operation, std::string message, std::vector<std::string> elements)
{
    std::cout << STOP << "\t\t\t|" << std::endl;
    std::cout <<         "\t\t\t---> " << getColor(operation) << message << ": [ ";
  int i = 0, j = 0;
  std::string array_elements = "", line = "";
  for(auto element : elements){
   line += element;
   array_elements += element;
    i++; j++;
    if(i < elements.size()){
      array_elements += ", ";
      if(line.size() >= 20){
        array_elements += "\n\t\t\t\t\t       º";
        line = "";
        j = 0;
      }
    }
  }
  std::cout << array_elements << " ]" << STOP << std::endl;
}

void Prettify::printError(std::string message)
{
  std::cout << std::endl;
  std::cout << ERROR << "[ERROR]: " << message
            << STOP << std::endl;
}

void Prettify::printWarningVector(std::string message, std::vector<std::string> vector)
{
  std::cout << std::endl;
  std::cout << WARNING << "[WARNING]: " << message;
  for(int i = 0; i < vector.size(); i++){
    if(vector.size()-1 == i){
      std::cout << vector[i];
    }else{
      std::cout << vector[i] << ", ";
    }
  }
  std::cout << STOP << std::endl;
}


void Prettify::printEnd(std::string message)
{
  std::cout << std::endl;
  std::cout << OK << "[DONE]: " << message
            << STOP << std::endl;
  std::cout << std::endl;
}

std::string Prettify::getColor(std::string operation)
{
  std::transform(operation.begin(), operation.end(), operation.begin(), ::toupper);
  if(operation == "INFO")
    return /*INFO*/"";
  if(operation == "ERROR")
    return ERROR;
  if(operation == "OK")
    return OK;
  if(operation == "WARNING")
    return WARNING;
  else
    return STOP;
}

void Prettify::printTitle(std::string message)
{
  std::cout << OK << "\n= = = > "<< message << " < = = = " << STOP << std::endl;
}
