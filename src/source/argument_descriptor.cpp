/*!*******************************************************************************************
 *  \file       argument_descriptor.cpp
 *  \brief      ArgumentDescriptor implementation file.
 *  \details    This file implements the ArgumentDescriptor class.
 *  \authors    Alberto Camporredondo.
 *  \copyright  Copyright 2017 Universidad Politecnica de Madrid (UPM)
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program. If not, see http://www.gnu.org/licenses/.
 ********************************************************************************/

#include "../include/argument_descriptor.h"

ArgumentDescriptor::ArgumentDescriptor()
{

}

ArgumentDescriptor::ArgumentDescriptor(std::string name,
                                       std::vector<std::string> allowed_values,
                                       int dimensions)
{
  this->name = name;
  this->allowed_values = allowed_values;
  this->dimensions = dimensions;
}

ArgumentDescriptor::~ArgumentDescriptor()
{

}



/*Functionality*/
bool ArgumentDescriptor::check_name(std::string argument_name)
{
  return argument_name == name;
}

std::tuple<bool, std::string> ArgumentDescriptor::check_string_format(std::string argument_value)
{
  bool found = false;
  std::for_each(allowed_values.begin(), allowed_values.end(),
                              [&](std::string allowed_value){
                                if(allowed_value == argument_value){
                                  found = true;
                                }
                              });
  if(!found)
    return std::make_tuple(false, "Argument value: \""+argument_value+"\" not possible ");
  else
    return std::make_tuple(true, "");
}

std::tuple<bool, std::string> ArgumentDescriptor::check_int_format(double number)
{
  int argument_dimension = 1;

  if(argument_dimension != this->dimensions){
    return std::make_tuple(false, "Dimensions \""+std::to_string(argument_dimension)+"\" do not match ");
  }

  if((number < atoi(allowed_values[0].c_str())) || (number > atoi(allowed_values[1].c_str()))){
    return std::make_tuple(false, "Value \""+std::to_string(number)+"\" out of range");
  }
  return std::make_tuple(true, "");
}

std::tuple<bool, std::string> ArgumentDescriptor::check_int_format(std::vector<double> argument_values)
{
  int argument_dimension = argument_values.size();

  if(argument_dimension != this->dimensions){
    return std::make_tuple(false, "Dimensions \""+std::to_string(argument_dimension)+"\" do not match ");
  }

  for(int number : argument_values){
    if((number < atoi(allowed_values[0].c_str())) || (number > atoi(allowed_values[1].c_str()))){
      return std::make_tuple(false, "Value \""+std::to_string(number)+"\" out of range");
    }
  }
  return std::make_tuple(true, "");
}

std::tuple<bool, std::string> ArgumentDescriptor::check_variable_format(std::vector<std::string> argument_variable)
{
  int argument_dimension = argument_variable.size();

  if(argument_dimension != this->dimensions){
    return std::make_tuple(false, "Dimensions \""+std::to_string(argument_dimension)+"\" do not match");
  }

  for(std::string variable : argument_variable){
    if(variable.find("+") != std::string::npos){
      
    }
  }
  return std::make_tuple(true,"");
}

/*Getters*/
std::string ArgumentDescriptor::getName()
{
  return name;
}

std::vector<std::string> ArgumentDescriptor::getAllowedValues()
{
  return allowed_values;
}

int ArgumentDescriptor::getDimensions()
{
  return dimensions;
}

/*Setters*/
void ArgumentDescriptor::setName(std::string name)
{
  this->name = name;
}

void ArgumentDescriptor::setAllowedValues(std::vector<std::string> allowed_values)
{
  this->allowed_values = allowed_values;
}

void ArgumentDescriptor::setDimensions(int dimensions)
{
  this->dimensions = dimensions;
}
