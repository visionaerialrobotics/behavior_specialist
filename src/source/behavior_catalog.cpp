/*!*******************************************************************************************
 *  \file       behavior_catalog.cpp
 *  \brief      BehaviorCatalog implementation file.
 *  \details    This file implements the BehaviorCatalog class.
 *  \authors    Alberto Camporredondo.
 *  \copyright  Copyright 2017 Universidad Politecnica de Madrid (UPM)
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program. If not, see http://www.gnu.org/licenses/.
 ********************************************************************************/

#include "../include/behavior_catalog.h"

#define BEHAVIOR_DESCRIPTORS "behavior_descriptors"
#define BEHAVIOR "behavior"
#define TIMEOUT "timeout"
#define CATEGORY "category"
#define CAPABILITIES "capabilities"
#define INCOMPATIBLE_BEHAVIORS "incompatible_behaviors"
#define INCOMPATIBLE_LISTS "incompatible_lists"

#define BEHAVIOR_LISTS "behavior_lists"
#define LIST "list"
#define BEHAVIORS "behaviors"

#define CAPABILITY_DESCRIPTORS "capability_descriptors"
#define CAPABILITY "capability"
#define PERMANENT_ACTIVE "permanent_active"
#define REQUIRED_CAPABILITIES "required_capabilities"
#define PROCESS_SEQUENCE "process_sequence"
#define INCOMPATIBLE_CAPABILITIES "incompatible_capabilities"

#define DEFAULT_BEHAVIOR_VALUES "default_behavior_values"

#define REACTIVE_ACTIVATION "reactive_activation"


BehaviorCatalog::BehaviorCatalog()
{

}

BehaviorCatalog::~BehaviorCatalog()
{

}


bool BehaviorCatalog::loadConfiguration(std::string path_file)
{
  config_file_path = path_file;

  if(behaviors_map.size() > 0 || capabilities_map.size() > 0){
    std::cout << "[INFO]: configuration already loaded" << std::endl;
    return true;
  }
  std::cout << "[INFO]: loading configuration" << std::endl;

  std::cout << "[INFO]: config_file path: " << path_file << std::endl;
  YAML::Node config_file;
  try{
    config_file = YAML::LoadFile(path_file);
    if(config_file.IsNull()){
      std::cout << ERROR << "[ERROR]: There is a problem with the file" << STOP << std::endl;
      return false;
    }
  }catch(YAML::Exception exception){
    prettify.printError(exception.what());
    return false;
  }

  std::string behavior_name, capability_name;
  int behavior_timeout, default_behavior_timeout;
  bool timeout_set_in_default = false, category_set_in_default = false;
  std::vector<ArgumentDescriptor> behavior_arguments;
  bool is_a_default_capability;
  std::string behavior_category, default_behavior_category;
  std::vector<std::string> behavior_incompatibilities, capability_incompatibilities;
  std::vector<std::string> behavior_capabilities, capability_processes;


  /*Default behavior values*/
  if(config_file[DEFAULT_BEHAVIOR_VALUES].IsDefined()){
    prettify.printTitle("Default behavior values");
    auto default_behavior_values = config_file[DEFAULT_BEHAVIOR_VALUES];

    /*Timeout*/
    default_behavior_timeout = default_behavior_values["timeout"].as<int>();
    timeout_set_in_default = true;
    prettify.printParent("INFO", "default behavior values");
    prettify.printChild("INFO", "timeout: {"+std::to_string(default_behavior_timeout)+"}");

    /*Category*/
    default_behavior_category = default_behavior_values["category"].as<std::string>();
    category_set_in_default = true;
    prettify.printChild("INFO", "category: {"+default_behavior_category+"}");
  }else{
    prettify.printParent("INFO", "Tag {default_behavior_values} is not defined");
  }

  /*Behavior descriptor*/
  if(config_file[BEHAVIOR_DESCRIPTORS].IsDefined()){
    prettify.printTitle("Behavior descriptors");

    for(auto behavior : config_file[BEHAVIOR_DESCRIPTORS]){

      behavior_name = behavior[BEHAVIOR].as<std::string>();
      prettify.printParent("INFO", "behavior: { "+behavior_name+" }");

      if(behavior[TIMEOUT].IsDefined()){
	behavior_timeout = behavior[TIMEOUT].as<int>();
	prettify.printChild("INFO", "timeout: { "+behavior[TIMEOUT].as<std::string>()+" }");
      }else{
        if(!timeout_set_in_default){
          prettify.printError("Timeout needs to be defined as default or specific for this behavior");
          return false;
        }
        behavior_timeout = default_behavior_timeout;
	prettify.printChild("WARNING", "{ timeout }: is not defined. Default value is { "+std::to_string(behavior_timeout)+" }" );
      }

      if(behavior[CATEGORY].IsDefined()){
	behavior_category = behavior[CATEGORY].as<std::string>();
	prettify.printChild("INFO", "category: { "+behavior_category+" }");
      }else{
        if(!category_set_in_default){
          prettify.printError("Category needs to be defined as default or specific for this behavior");
          return false;
        }
        behavior_category = default_behavior_category;
	prettify.printChild("WARNING", "{ category }: is not defined. Default value is { "+behavior_category+" }" );
      }

      if(behavior[CAPABILITIES].IsDefined()){
	for(auto capability : behavior[CAPABILITIES]){
	  std::string capability_lowercase = capability.as<std::string>();
	  behavior_capabilities.push_back(capability_lowercase);
	}
	prettify.printChildList("INFO", "capabilities", behavior_capabilities);
      }else{
	prettify.printChild("INFO", "{ capabilities }: is not defined." );
      }

      if(behavior[INCOMPATIBLE_BEHAVIORS].IsDefined()){
	for(auto behavior_incompatible : behavior[INCOMPATIBLE_BEHAVIORS]){
	  behavior_incompatibilities.push_back(behavior_incompatible.as<std::string>());
	}
	prettify.printChildList("INFO", "incompatibilities", behavior_incompatibilities);
      }else{
	prettify.printChild("INFO", "Tag: { incompatible_behaviors } is not define.");
      }

      if(behavior[INCOMPATIBLE_LISTS].IsDefined()){
	for(auto incompatible_list : behavior[INCOMPATIBLE_LISTS]){
	  if(config_file[BEHAVIOR_LISTS].IsDefined()){
	    for(auto list : config_file[BEHAVIOR_LISTS]){
	      if(list[LIST].as<std::string>() == incompatible_list.as<std::string>()){
		for(auto elements : list[BEHAVIORS]){
		  behavior_incompatibilities.push_back(elements.as<std::string>());
		}
	      }
	    }
	  }else{
	    prettify.printChild("ERROR", "Tag: { behavior_lists }: is not defined." );
	    return false;
	  }
	}
	prettify.printChildList("INFO", "incompatible_list", behavior_incompatibilities);
      }else{
	prettify.printChild("INFO", "Tag: { incompatible_lists }: is not defined." );
      }

      if(behavior["arguments"].IsDefined()){
        prettify.printChild("INFO", "arguments");
        for(auto argument : behavior["arguments"]){

          if(!argument["argument"].IsDefined()){
            prettify.printGrandChild("ERROR", "Tag: { argument }: is not defined." );
            return false;
          }

          if(!argument["allowed_values"].IsDefined()){
            prettify.printGrandChild("ERROR", "Tag: { allowed_values }: is not defined." );
            return false;
          }

          int dimensions = 0;
          if(!argument["dimensions"].IsDefined()){
            prettify.printGrandChild("INFO", "Tag: {dimensions} is not defined. Setting value to 1.");
            dimensions = 1;
          }
          else{
            dimensions = argument["dimensions"].as<int>();
            prettify.printGrandChild("INFO", "dimensions: {"+std::to_string(dimensions)+"}");
          }

          std::vector<std::string> values;
          for(auto value : argument["allowed_values"])
            values.push_back(value.as<std::string>());

          prettify.printGrandChild("INFO", argument["argument"].as<std::string>());
          prettify.printGrandChildList("INFO", "allowed_values", values);

          behavior_arguments.push_back(ArgumentDescriptor(argument["argument"].as<std::string>(),
                                                       values, dimensions));
        }
      }
      else
        prettify.printChild("INFO", "Tag: {arguments} is not defined");

      behaviors_map.insert({behavior_name, BehaviorDescriptor(behavior_name,
							      behavior_capabilities,
							      behavior_incompatibilities,
							      behavior_category,
							      behavior_timeout,
                                                              behavior_arguments)
			  });
      behavior_capabilities.clear();
      behavior_incompatibilities.clear();
      behavior_arguments.clear();
    }
  }
  else{
    prettify.printError("Tag { "+std::string(BEHAVIOR_DESCRIPTORS)+" }: is not defined ");
    return false;
  }

  /*Default behaviors*/
  if(config_file[REACTIVE_ACTIVATION].IsDefined()){
    prettify.printTitle(std::string(REACTIVE_ACTIVATION));
    for(auto behavior : config_file[REACTIVE_ACTIVATION]){
      if(!behavior["behavior"].IsDefined()){
        prettify.printError("Tag [behavior] is not defined.");
        return false;
      }
      auto behavior_name = behavior["behavior"].as<std::string>();
      prettify.printParent("INFO", "behavior: ["+behavior_name+"]");

      std::string reactive_argument = "";
      if(behavior["arguments"].IsDefined()){
        reactive_argument = behavior["arguments"].as<std::string>();
        prettify.printChild("INFO", "arguments: ["+reactive_argument+"]");
      }
      
      std::string behavior_condition = "";
      if(behavior["condition"].IsDefined()){
        behavior_condition = behavior["condition"].as<std::string>();
        prettify.printChild("INFO", "condition: ["+behavior_condition+"]");
      }

      if(!behavior["priority"].IsDefined()){
        prettify.printError("Tag [priority] is not defined.");
        return false;
      }

      auto behavior_priority = behavior["priority"].as<std::string>();
      prettify.printChild("INFO", "priority: ["+behavior_priority+"]");
      reactive_behaviors_map.insert({behavior_name, ReactiveBehaviorDescriptor(behavior_name,
									       reactive_argument,
									       behavior_condition,
									       behavior_priority)
	    });
    }
  }
  else{
    prettify.printParent("INFO", std::string(REACTIVE_ACTIVATION)+": NOT defined");
    //return false;
  }

  /*Capability descriptors*/
  if(config_file[CAPABILITY_DESCRIPTORS].IsDefined()){
    prettify.printTitle("Capability descriptor");

    std::vector<std::string> active_processes; //Usefull for checking if a processes is repeated along different capabilities

    for(auto capability : config_file[CAPABILITY_DESCRIPTORS]){

      capability_name = capability[CAPABILITY].as<std::string>();
      prettify.printParent("INFO", "capability_name:{ "+capability_name+" }");

      if(capability[PERMANENT_ACTIVE].IsDefined()){
	is_a_default_capability = capability[PERMANENT_ACTIVE].as<bool>();
	prettify.printChild("INFO", "default:{ "+std::to_string(is_a_default_capability)+" }");
      }else{
	is_a_default_capability = false;
	prettify.printChild("WARNING", "Tag: { basic } in capabilities: is not defined. Default value is { "+std::to_string(is_a_default_capability)+" }" );
      }

      if(capability[PROCESS_SEQUENCE].IsDefined()){
	for(auto process : capability[PROCESS_SEQUENCE]){

	  if(std::find(active_processes.begin(), active_processes.end(), process.as<std::string>())
	     != active_processes.end()){
	    std::string msg = "Capability ["+capability_name+"]: contains a process already"
	      +" assigned to another capability. Processes must be assigned once.";
	    prettify.printError(msg);
	    return false;
	  }

	  active_processes.push_back(process.as<std::string>());
	  capability_processes.push_back(process.as<std::string>());

	}
	prettify.printChildList("INFO", "processes", capability_processes);
      }
      else
	prettify.printChild("INFO", "Tag: { processes } is not defined.");

      //incompatible capabilities
      if(capability[INCOMPATIBLE_CAPABILITIES].IsDefined()){
	for(auto incompatibility : capability[INCOMPATIBLE_CAPABILITIES]){
	  capability_incompatibilities.push_back(incompatibility.as<std::string>());
	}
	prettify.printChildList("INFO", "incompatibilities", capability_incompatibilities);
      }else{
	prettify.printChild("INFO", "Tag: { incompatible_capabilities } is not defined");
      }

      capabilities_map.insert({capability_name, CapabilityDescriptor(capability_name,
								     is_a_default_capability,
								     capability_processes,
								     capability_incompatibilities)
			      });
      capability_incompatibilities.clear();
      capability_processes.clear();
    }
  }
  else{
    prettify.printError("Tag: { "+std::string(CAPABILITY_DESCRIPTORS)+" } is not defined");
    return false;
  }

  /*bool processes_launched; std::vector<std::string> processes_not_launched;
  std::tie(processes_launched, processes_not_launched) = processesLaunched();
  if(!processes_launched){
    prettify.printWarningVector("The following processes are not launched: ", processes_not_launched);
  }

  bool behaviors_launched; std::vector<std::string> behaviors_not_launched;
  std::tie(behaviors_launched, processes_not_launched) = behaviorsLaunched();
  if(!behaviors_launched){
    prettify.printWarningVector("The following behaviors are not launched: ", behaviors_not_launched);
    }
    else{*/
    prettify.printEnd("Behavior Configuration has been loaded");
    //  }
  return true;
}


void BehaviorCatalog::eraseConfiguration()
{
  behaviors_map.clear();
  capabilities_map.clear();
  reactive_behaviors_map.clear();
}


/*Managing maps*/
std::tuple<bool, std::string, BehaviorDescriptor>
        BehaviorCatalog::getBehaviorDescriptor(std::string name)
{
  std::transform(name.begin(), name.end(), name.begin(), ::toupper);
  if(behaviors_map.size() <= 0){
    return std::make_tuple(false, "No config loaded",  BehaviorDescriptor());
  }

  if(behaviors_map.count(name) == 0){
    return std::make_tuple(false, name+" has not been found in the configuration",  BehaviorDescriptor());
  }

  return std::make_tuple(true, "", behaviors_map.find(name)->second);
}

std::tuple<bool, std::string, ReactiveBehaviorDescriptor>
         BehaviorCatalog::getReactiveBehaviorDescriptor(std::string name)
{
  std::transform(name.begin(), name.end(), name.begin(), ::toupper);
  if(reactive_behaviors_map.size() <= 0)
  {
    return std::make_tuple(false, "No config loaded", ReactiveBehaviorDescriptor());
  }

  if(reactive_behaviors_map.count(name) == 0)
  {
    return std::make_tuple(false, name+" has not been found in the configuration", ReactiveBehaviorDescriptor());
  }

  return std::make_tuple(true, "", reactive_behaviors_map.find(name)->second);
}

std::tuple<bool, std::string, CapabilityDescriptor>
          BehaviorCatalog::getCapabilityDescriptor(std::string name)
{
  std::transform(name.begin(), name.end(), name.begin(), ::toupper);
  if(capabilities_map.size() <= 0){
    return std::make_tuple(false, "No configuration has been loaded", CapabilityDescriptor());
  }
  if(capabilities_map.count(name) == 0){
    return std::make_tuple(false, name+" has not been found in the configuration", CapabilityDescriptor());
  }
  return std::make_tuple(true, "", capabilities_map.find(name)->second);
}

std::vector<ReactiveBehaviorDescriptor> BehaviorCatalog::getHighPriorityReactiveBehaviors()
{
  std::vector<ReactiveBehaviorDescriptor> reactive_behaviors;
  for(auto behavior : reactive_behaviors_map){
    if(behavior.second.hasHigherPriority()){
      reactive_behaviors.push_back(behavior.second);
    }
  }
  return reactive_behaviors;
}

std::vector<ReactiveBehaviorDescriptor> BehaviorCatalog::getLowPriorityReactiveBehaviors()
{
  std::vector<ReactiveBehaviorDescriptor> reactive_behaviors;
  for(auto behavior : reactive_behaviors_map)
  {
    if(behavior.second.hasLowerPriority())
    {
      reactive_behaviors.push_back(behavior.second);
    }
  }
  return reactive_behaviors;
}


std::vector<CapabilityDescriptor> BehaviorCatalog::getDefaultCapabilityDescriptors()
{
  std::vector<CapabilityDescriptor> basic_capabilities;
  for(auto capability : capabilities_map){
    if(capability.second.isBasic()){
      std::cout << "Retrieving default Capability: " << capability.first << std::endl;
      basic_capabilities.push_back(capability.second);
    }
  }
  return basic_capabilities;
}


std::tuple<bool, std::string, std::vector<CapabilityDescriptor> >
        BehaviorCatalog::getCapabilities(std::string behavior_name)
{
  std::vector<CapabilityDescriptor> empty_vector;
  std::transform(behavior_name.begin(), behavior_name.end(), behavior_name.begin(), ::toupper);

  bool found; std::string error_message; BehaviorDescriptor behavior;
  std::tie(found, error_message, behavior) = getBehaviorDescriptor(behavior_name);

  if(found){
    std::vector<CapabilityDescriptor> capabilities;
    for(auto capability_name : behavior.getCapabilities()){
      bool found; std::string error_message; CapabilityDescriptor capability;
      std::tie(found, error_message, capability) = getCapabilityDescriptor(capability_name);

      if(found)
        capabilities.push_back(capability);
      else
        return std::make_tuple(false, error_message, empty_vector);
    }
    return std::make_tuple(true, "", capabilities);
  }
  return std::make_tuple(false, error_message, empty_vector);
}

std::vector<BehaviorDescriptor> BehaviorCatalog::getBehaviors()
{
  std::vector<BehaviorDescriptor> available_behaviors;

  for(auto behavior : behaviors_map){
    available_behaviors.push_back(behavior.second);
  }

  return available_behaviors;
}

void BehaviorCatalog::checkProcesses(ros::V_string available_processes, std::string node_namespace)
{
  std::vector<std::string> processes;
  std::for_each(available_processes.begin(), available_processes.end(),
		[&](std::string available_process){
		  std::string target = "/"+node_namespace+"/";
		  int target_size = target.size();

		  if(std::regex_match(available_process, std::regex("\\/drone\\d\\/.*"))){
		    //Follows /drone1/...
		    std::string process = available_process.substr(target_size);
		    processes.push_back(process);
		  }else{
		    //Does not follows /drone1/...
		    std::string process = available_process.substr(1);
		    processes.push_back(process);
		  }
		});
  
  std::vector<std::string> processes_in_catalog = getProcessesInCatalog();
  std::vector<std::string> unavailable_processes = checkUnavailability(processes, processes_in_catalog);
  
  if(unavailable_processes.size() > 0){
    prettify.printWarningVector("The following processes are set in the catalog but not launched: ", unavailable_processes);
  }else{
    std::cout << "PROCESSES LOADED CORRECTLY" << std::endl;
  }
  
}

void BehaviorCatalog::checkBehaviors(ros::V_string available_behaviors, std::string node_namespace)
{
  std::vector<std::string> behaviors;
  std::for_each(available_behaviors.begin(), available_behaviors.end(),
		[&](std::string available_behaviors){
		  std::string target = "/"+node_namespace+"/";
		  int target_size = target.size();

		  if(std::regex_match(available_behaviors, std::regex("\\/drone\\d\\/.*"))){
		    //Follows /drone1/...
		    std::string behavior = available_behaviors.substr(target_size);
		    behaviors.push_back(behavior);
		  }else{
		    //Does not follows /drone1/...
		    std::string behavior = available_behaviors.substr(1);
		    behaviors.push_back(behavior);
		  }
		});

  std::vector<std::string> behaviors_in_catalog;
  for(auto behavior_pair : behaviors_map){
    std::string behavior = behavior_pair.first;
    std::transform(behavior.begin(), behavior.end(), behavior.begin(), ::tolower);
    behavior = "behavior_"+behavior;
    behaviors_in_catalog.push_back(behavior);
  }  
  std::vector<std::string> unavailable_behaviors = checkUnavailability(behaviors, behaviors_in_catalog);
  
  if(unavailable_behaviors.size() > 0){
    prettify.printWarningVector("The following behaviors are set in the catalog but not launched: ", unavailable_behaviors);
  }
}

std::vector<std::string> BehaviorCatalog::checkUnavailability(std::vector<std::string> processes_launched,
							      std::vector<std::string> processes_in_catalog)
{
  std::vector<std::string> processes_unavailable;
  for(auto process_in_catalog : processes_in_catalog){
    if(!processIsLaunched(process_in_catalog, processes_launched)){
      processes_unavailable.push_back(process_in_catalog);
    }
  }
  return processes_unavailable;
}

bool BehaviorCatalog::processIsLaunched(std::string process_in_catalog, std::vector<std::string> processes_launched)
{
  for(auto process_launched : processes_launched){
    if(process_in_catalog == process_launched){
      return true;
    }
  }
  return false;
}


std::vector<std::string> BehaviorCatalog::getProcessesInCatalog()
{
  std::vector<std::string> processes_in_catalog;
  for (auto capability : capabilities_map){
    std::vector<std::string> processes = capability.second.getProcesses();

    for(auto process : processes){
      processes_in_catalog.push_back(process);
    }
  }
  return processes_in_catalog;
}
