/*!*******************************************************************************************
 *  \file       behavior_descriptor.cpp
 *  \brief      BehaviorDescriptor implementation file.
 *  \details    This file implements the BehaviorDescriptor class.
 *  \authors    Alberto Camporredondo.
 *  \copyright  Copyright 2017 Universidad Politecnica de Madrid (UPM)
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program. If not, see http://www.gnu.org/licenses/.
 ********************************************************************************/

#include "../include/behavior_descriptor.h"

BehaviorDescriptor::BehaviorDescriptor()
{

}

BehaviorDescriptor::BehaviorDescriptor(std::string name,
				       std::vector<std::string> capabilities,
				       std::vector<std::string> incompatibilities,
				       std::string category, int timeout)
{
  BehaviorDescriptor(name, capabilities, incompatibilities, category,
                     timeout, std::vector<ArgumentDescriptor>());
}

BehaviorDescriptor::BehaviorDescriptor(std::string name,
                                       std::vector<std::string> capabilities,
                                       std::vector<std::string> incompatibilities,
                                       std::string category, int timeout,
                                       std::vector<ArgumentDescriptor> arguments)
{
  this->name = name;
  this->capabilities = capabilities;
  this->incompatibilities = incompatibilities;
  this->category = category;
  this->timeout = timeout;
  this->arguments = arguments;
}

BehaviorDescriptor::~BehaviorDescriptor()
{

}



/*Functionality*/
aerostack_msgs::BehaviorCommand BehaviorDescriptor::serialize()
{
  aerostack_msgs::BehaviorCommand behavior_msg;
  behavior_msg.name = name;

  return behavior_msg;
}

/*Getter's*/
std::string BehaviorDescriptor::getName()
{
  return name;
}

std::vector<ArgumentDescriptor> BehaviorDescriptor::getArguments()
{
  return arguments;
}

int BehaviorDescriptor::getTimeout(){
  return timeout;
}

std::string BehaviorDescriptor::getCategory(){
  return category;
}

std::vector<std::string> BehaviorDescriptor::getCapabilities()
{
  return capabilities;
}

std::vector<std::string> BehaviorDescriptor::getIncompatibilities()
{
  return incompatibilities;
}


/*Setter's*/
void BehaviorDescriptor::setName(std::string name){
  this->name = name;
}

void BehaviorDescriptor::setTimeout(int timeout){
  this->timeout = timeout;
}

void BehaviorDescriptor::setCategory(std::string value){
  category = value;
}

void BehaviorDescriptor::setIncompatibilities(std::vector<std::string> incompatibilities){
  this->incompatibilities = incompatibilities;
}

void BehaviorDescriptor::setCapabilities(std::vector<std::string> capabilities){
  this->capabilities = capabilities;
}
